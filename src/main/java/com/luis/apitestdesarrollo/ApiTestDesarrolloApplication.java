package com.luis.apitestdesarrollo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTestDesarrolloApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiTestDesarrolloApplication.class, args);
    }

}
