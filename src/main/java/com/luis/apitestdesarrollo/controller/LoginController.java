package com.luis.apitestdesarrollo.controller;

import com.google.gson.Gson;
import com.luis.apitestdesarrollo.model.dto.LoginDto;
import com.luis.apitestdesarrollo.model.dto.ResponseDto;
import com.luis.apitestdesarrollo.model.repository.dao.UsuarioDao;
import com.luis.apitestdesarrollo.model.request.Login;
import lombok.extern.log4j.Log4j2;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.keys.AesKey;
import org.jose4j.lang.ByteUtil;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Key;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/v1/seguridad")
@Log4j2
public class LoginController {

    private final UsuarioDao usuarioDao;

    @Autowired
    public LoginController(UsuarioDao usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> login(@RequestBody Login login) {
        Gson gson = new Gson();

       log.info("Datos de usuario {}", login);

        if (this.usuarioDao.findUsuarioByUsernameAndClave(login.getUsuario(), login.getClave()).isEmpty()) {
            log.warn("Usuario o clave invalida");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    gson.toJson(ResponseDto.builder()
                            .message("Usuario o clave invalida")
                            .build())
            );
        }

        Key key = new AesKey(ByteUtil.randomBytes(16));
        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setPayload("Hello World!");
        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
        jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        jwe.setKey(key);
        try {
            LoginDto loginDto = new LoginDto();
            loginDto.setAuthorization(jwe.getCompactSerialization());

            log.info("Respuesta autorizacion {}", loginDto);

            return ResponseEntity.ok().body(gson.toJson(loginDto));
        } catch (JoseException e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }

}
