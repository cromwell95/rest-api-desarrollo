package com.luis.apitestdesarrollo.controller;

import com.google.gson.Gson;
import com.luis.apitestdesarrollo.model.dto.ResponseDto;
import com.luis.apitestdesarrollo.model.dto.UsuarioDto;
import com.luis.apitestdesarrollo.model.repository.dao.UsuarioDao;
import com.luis.apitestdesarrollo.model.repository.entity.Usuario;
import com.luis.apitestdesarrollo.model.request.ReqUsuario;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/v1/usuario")
@Log4j2
public class UsuarioController {

    private final UsuarioDao usuarioDao;
    private final Gson gson;
    private final ModelMapper modelMapper;

    @Autowired
    public UsuarioController(UsuarioDao usuarioDao) {
        this.usuarioDao = usuarioDao;
        gson = new Gson();
        modelMapper = new ModelMapper();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(@RequestHeader(name = HttpHeaders.AUTHORIZATION) String authorization) {
        if (authorization == null || authorization.isEmpty()) {
            log.warn("Usuario o clave invalida");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    gson.toJson(ResponseDto.builder()
                            .message("Usuario o clave invalida")
                            .build())
            );
        }

        List<UsuarioDto> usuarioDto = this.usuarioDao.findAll().stream()
                .map(usuario -> modelMapper.map(usuario, UsuarioDto.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(gson.toJson(usuarioDto));
    }

    @GetMapping(value = "/byId", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@RequestHeader(name = HttpHeaders.AUTHORIZATION) String authorization,
                                         @RequestParam
                                         @NotNull(message = "Debe indicar el id del usuario")
                                         Long idUsuario) {
        if (authorization == null || authorization.isEmpty()) {
            log.warn("Usuario o clave invalida");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    gson.toJson(ResponseDto.builder()
                            .message("Usuario o clave invalida")
                            .build())
            );
        }

        Optional<Usuario> usuarioOptional = this.usuarioDao.findById(idUsuario);
        if (usuarioOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        UsuarioDto usuarioDto = this.modelMapper.map(usuarioOptional.get(), UsuarioDto.class);

        return ResponseEntity.ok().body(gson.toJson(usuarioDto));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> save(@RequestHeader(name = HttpHeaders.AUTHORIZATION) String authorization,
                                       @Valid @RequestBody ReqUsuario request) {
        if (authorization == null || authorization.isEmpty()) {
            log.warn("Usuario o clave invalida");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    gson.toJson(ResponseDto.builder()
                            .message("Usuario o clave invalida")
                            .build())
            );
        }

        Usuario usuarioToSave = this.modelMapper.map(request, Usuario.class);
        usuarioToSave.setFechaRegistro(LocalDateTime.now());
        usuarioToSave.setFechaNacimiento(LocalDate.parse(request.getFechaNacimiento()));

        this.usuarioDao.save(usuarioToSave);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> delete(@RequestHeader(name = HttpHeaders.AUTHORIZATION) String authorization,
                                         @RequestParam
                                         @NotNull(message = "Debe indicar el id del usuario")
                                         Long idUsuario) {
        if (authorization == null || authorization.isEmpty()) {
            log.warn("Usuario o clave invalida");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    gson.toJson(ResponseDto.builder()
                            .message("Usuario o clave invalida")
                            .build())
            );
        }

        if (this.usuarioDao.existsById(idUsuario)) {
            this.usuarioDao.deleteById(idUsuario);
        }

        return ResponseEntity.ok().build();
    }

}
