package com.luis.apitestdesarrollo;

import com.google.gson.Gson;
import com.luis.apitestdesarrollo.model.repository.dao.UsuarioDao;
import com.luis.apitestdesarrollo.model.repository.entity.Usuario;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
@Log4j2
public class EventListenerInitialize {

    private final UsuarioDao usuarioDao;

    @Autowired
    public EventListenerInitialize(UsuarioDao usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initializeDatabase() {
        Usuario usuario = new Usuario();
        usuario.setPrimerNombre("Luis");
        usuario.setSegundoNombre("Alberto");
        usuario.setApellidoPaterno("Bravo");
        usuario.setApellidoMaterno("Alvarado");
        usuario.setGenero("M");
        usuario.setUsername("lbravoa");
        usuario.setClave("guest");
        usuario.setFechaNacimiento(LocalDate.parse("1995-10-25"));
        usuario.setFechaRegistro(LocalDateTime.now());

        this.usuarioDao.save(usuario);

        usuario = new Usuario();
        usuario.setPrimerNombre("Maria");
        usuario.setSegundoNombre("de Lourdes");
        usuario.setApellidoPaterno("Ponce");
        usuario.setApellidoMaterno("Ochoa");
        usuario.setGenero("F");
        usuario.setUsername("mponceo");
        usuario.setClave("admin");
        usuario.setFechaNacimiento(LocalDate.parse("2001-07-09"));
        usuario.setFechaRegistro(LocalDateTime.now());

        this.usuarioDao.save(usuario);

        Gson gson = new Gson();

        log.info("Datos en base {}", this.usuarioDao.findAll());
    }

}
