package com.luis.apitestdesarrollo.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class Login {

    @NotBlank(message = "Debe ingresar el usuario")
    private String usuario;

    @NotBlank(message = "Debe ingresar la clave")
    private String clave;

}
