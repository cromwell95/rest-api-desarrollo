package com.luis.apitestdesarrollo.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ReqUsuario {

    @NotBlank(message = "Debe ingresar su primer nombre")
    private String primerNombre;

    @NotBlank(message = "Debe ingresar su segundo nombre")
    private String segundoNombre;

    @NotBlank(message = "Debe ingresar su apellido paterno")
    private String apellidoPaterno;

    @NotBlank(message = "Debe ingresar su apellido materno")
    private String apellidoMaterno;

    @NotBlank(message = "Debe seleccionar su genero M o F")
    @Size(min = 1, max = 1, message = "Solo se permite 1 caracter")
    @Pattern(regexp = "[M|F]", message = "Solo se permite M o F")
    private String genero;

    @NotBlank(message = "Debe ingresar su usuario de acceso a la plataforma")
    private String username;

    @NotBlank(message = "Debe ingresar su clave de acceso a la plataforma")
    private String clave;

    @NotBlank(message = "Debe ingresar su fecha de nacimiento")
    private String fechaNacimiento;

}
