package com.luis.apitestdesarrollo.model.dto;

import lombok.Builder;

@Builder
public class ResponseDto {

    private String message;

}
