package com.luis.apitestdesarrollo.model.dto;

import lombok.Data;

@Data
public class UsuarioDto {

    private Long idUsuario;
    private String primerNombre;
    private String segundoNombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String genero;
    private String username;
    private String clave;
    private String fechaNacimiento;

}
