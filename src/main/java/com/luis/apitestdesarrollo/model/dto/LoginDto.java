package com.luis.apitestdesarrollo.model.dto;

import lombok.Data;

@Data
public class LoginDto {

    private String authorization;

}
