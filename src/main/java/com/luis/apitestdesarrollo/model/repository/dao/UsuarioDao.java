package com.luis.apitestdesarrollo.model.repository.dao;

import com.luis.apitestdesarrollo.model.repository.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsuarioDao extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findUsuarioByUsernameAndClave(String usuario, String clave);

}
