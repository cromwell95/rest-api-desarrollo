package com.luis.apitestdesarrollo.model.repository.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUsuario;

    private String primerNombre;

    private String segundoNombre;

    private String apellidoPaterno;

    private String apellidoMaterno;

    private String genero;

    private String username;

    private String clave;

    private LocalDate fechaNacimiento;

    private LocalDateTime fechaRegistro;

}
